package app;

import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainWindow extends JFrame implements ActionListener, WorkerDoneListener{
    private JTextField tfInputFolder;
    private JButton btnChooseInputFolder;
    private JButton btnChooseOutputFile;
    private JTextField tfOutputFile;
    private JProgressBar progressBarMain;
    private JPanel windowPanel;
    private JButton btnGenerateFileTree;
    private JLabel progressLabel;

    private boolean taskIsRunning;
    private GenerateFileTreeWorker bgWorker;

    private Dimension windowDimensions = new Dimension(500, 200);

    public MainWindow(){
        setTitle("File Tree Generator");
        setContentPane(windowPanel);

        setSize(windowDimensions);
        setMinimumSize(windowDimensions);
        setMaximumSize(windowDimensions);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        taskIsRunning = false;

        // Listeners
        btnChooseInputFolder.addActionListener(this);
        btnChooseOutputFile.addActionListener(this);
        btnGenerateFileTree.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == btnChooseInputFolder && !taskIsRunning){
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Please choose the initial directory");
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fc.setAcceptAllFileFilterUsed(false);

            if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
                tfInputFolder.setText(fc.getSelectedFile().getAbsolutePath());
            }
        }

        if(e.getSource() == btnChooseOutputFile && !taskIsRunning){
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Please choose the output file");
            //fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fc.setSelectedFile(new File("file_tree.csv"));
            fc.setDialogType(JFileChooser.SAVE_DIALOG);

            if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                tfOutputFile.setText(fc.getSelectedFile().getAbsolutePath());
            }
        }

        if(e.getSource() == btnGenerateFileTree && !taskIsRunning){
            startTask();
        }else if(e.getSource() == btnGenerateFileTree && taskIsRunning){
            stopTask();
        }
    }

    private void startTask(){
        Path inputDir = Paths.get(tfInputFolder.getText());
        Path outputFile = Paths.get(tfOutputFile.getText());

        if(inputDir.toString().length() == 0 || !Files.isDirectory(inputDir)){
            JOptionPane.showMessageDialog(this,"Please specify a valid input directory.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if(outputFile.toString().length() == 0 || Files.isDirectory(outputFile)){
            JOptionPane.showMessageDialog(this,"Please specify a valid output file.","Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Start the task
        taskIsRunning = true;
        btnChooseInputFolder.setEnabled(false);
        btnChooseOutputFile.setEnabled(false);
        btnGenerateFileTree.setText("Cancel the running task");


        bgWorker = new GenerateFileTreeWorker(inputDir.toString(), outputFile.toString(), progressLabel, progressBarMain);
        bgWorker.setWorkerDoneListener(this);
        bgWorker.execute();
    }

    private void stopTask(){
        bgWorker.cancel(true);
    }

    @Override
    public void onWorkerDone() {
        taskIsRunning = false;
        btnChooseInputFolder.setEnabled(true);
        btnChooseOutputFile.setEnabled(true);
        btnGenerateFileTree.setText("Generate the file tree");
        progressLabel.setText("Ready for the next task.");

        progressBarMain.setValue(0);
    }
}
