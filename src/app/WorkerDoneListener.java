package app;

interface WorkerDoneListener {
    void onWorkerDone();
}
