package app;


import javax.swing.*;

public class FileTreeGenerator  {
    private static JFrame mainFrame;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | UnsupportedLookAndFeelException | IllegalAccessException ex){
            // Do nothing
        }

        mainFrame = new MainWindow();
    }

    public static JFrame getMainFrame() {
        return mainFrame;
    }
}
