package app;

import csvWriter.CsvStringOnlyWriter;
import filesTreeGenerator.CancelationDispatcher;
import filesTreeGenerator.FileInfo;
import filesTreeGenerator.FilesTreeGenerator;
import filesTreeGenerator.StatusCallbackHandler;
import hashCalculators.HashCalculatorSHA1;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenerateFileTreeWorker extends SwingWorker<Boolean, WorkerInternalMessage> implements StatusCallbackHandler, CancelationDispatcher {
    private WorkerDoneListener workerDoneListener;
    private String inputDir;
    private String outputFile;
    private JLabel statusLabel;
    private JProgressBar progressBar;

    public GenerateFileTreeWorker(String inputDir, String outputFile, JLabel statusLabel, JProgressBar progressBar){
        this.inputDir = inputDir;
        this.outputFile = outputFile;

        this.statusLabel = statusLabel;
        this.progressBar = progressBar;
    }

    public void setWorkerDoneListener(WorkerDoneListener workerDoneListener) {
        this.workerDoneListener = workerDoneListener;
    }

    @Override
    protected Boolean doInBackground() {
        publish(new WorkerInternalMessage(WorkerInternalMessage.type.STATUS_UPDATE, "Starting the parser."));

        FilesTreeGenerator ftg = new FilesTreeGenerator(inputDir);
        ftg.setHashCalculator(new HashCalculatorSHA1());
        ftg.setCallbackHandler(this);
        ftg.setCancelationDispatcher(this);

        try {
            ArrayList<FileInfo> fileList = ftg.getTree();

            if(isCancelled()){
                return false;
            }

            // Save the file
            CsvStringOnlyWriter csvWriter = new CsvStringOnlyWriter(outputFile);

            csvWriter.addRow(FileInfo.getFieldsName());
            fileList.forEach(file -> csvWriter.addRow(file.getFields()));

            csvWriter.write();

            publish(new WorkerInternalMessage(WorkerInternalMessage.type.TASK_FINISHED_SUCCESS, "Task finished. The resulted file tree was written into the output file."));
        } catch (IOException ex){
            publish(new WorkerInternalMessage(
                    WorkerInternalMessage.type.EXCEPTION_OCCURRED, ex.getMessage()
            ));
        }

        return true;
    }

    @Override
    public void filesAnalyzed(int number, int total) {
        publish(new WorkerInternalMessage(
                WorkerInternalMessage.type.STATUS_UPDATE, "Analyzing file " + number + " / " + total
        ));

        publish(new WorkerInternalMessage(
                WorkerInternalMessage.type.PROGRESSBAR_UPDATE, "", new int[]{number, total}
        ));
    }

    @Override
    protected void process(List<WorkerInternalMessage> chunks) {
        super.process(chunks);

        chunks.forEach((WorkerInternalMessage msg) -> {
            if(msg.getMessageType() == WorkerInternalMessage.type.STATUS_UPDATE){
                statusLabel.setText(msg.getMessageText());
            }

            if(msg.getMessageType() == WorkerInternalMessage.type.PROGRESSBAR_UPDATE){
                int[] complementaryData = msg.getComplementaryIntData();

                progressBar.setMinimum(0);
                progressBar.setMaximum(complementaryData[1]);

                progressBar.setValue(complementaryData[0]);
            }

            if(msg.getMessageType() == WorkerInternalMessage.type.EXCEPTION_OCCURRED){
                JOptionPane.showMessageDialog(
                        FileTreeGenerator.getMainFrame(),
                        msg.getMessageText(),
                        "Exception occurred",
                        JOptionPane.ERROR_MESSAGE
                );
            }

            if(msg.getMessageType() == WorkerInternalMessage.type.TASK_FINISHED_SUCCESS){
                JOptionPane.showMessageDialog(
                        FileTreeGenerator.getMainFrame(),
                        msg.getMessageText(),
                        "Info",
                        JOptionPane.INFORMATION_MESSAGE
                );
            }
        });
    }

    @Override
    protected void done() {
        super.done();

        if(workerDoneListener != null){
            workerDoneListener.onWorkerDone();
        }
    }

    @Override
    public boolean isTaskCanceled() {
        return isCancelled();
    }
}

class WorkerInternalMessage {
    public enum type {
            STATUS_UPDATE, PROGRESSBAR_UPDATE, EXCEPTION_OCCURRED, TASK_FINISHED_SUCCESS
    }
    public final int COMPLEMENTARY_INT_DATA_SIZE = 10;
    private type messageType;
    private String messageText;
    private int complementaryIntData[] = new int[COMPLEMENTARY_INT_DATA_SIZE];

    public WorkerInternalMessage(type messageType, String messageText){
        this.messageType = messageType;
        this.messageText = messageText;
    }

    public WorkerInternalMessage(type messageType, String messageText, int[] complementaryIntData){
        this.messageType = messageType;
        this.messageText = messageText;

        for(int i = 0; i < Math.min(COMPLEMENTARY_INT_DATA_SIZE, complementaryIntData.length); i++){
            this.complementaryIntData[i] = complementaryIntData[i];
        }
    }

    public type getMessageType() {
        return messageType;
    }

    public String getMessageText() {
        return messageText;
    }

    public int[] getComplementaryIntData() {
        return complementaryIntData;
    }
}