package hashCalculators;

import filesTreeGenerator.FileHashCalculator;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashCalculatorSHA1 implements FileHashCalculator {
    @Override
    public String computeHashForFile(String filePath) throws IOException {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");

            try(BufferedInputStream buffInputStr = new BufferedInputStream(new FileInputStream(filePath))){
                int n = 0;

                byte[] buffer = new byte[8192];
                while(n != -1){
                    n = buffInputStr.read(buffer);
                    if(n > 0){
                        digest.update(buffer, 0 ,n);
                    }
                }
            }

            return DatatypeConverter.printHexBinary(digest.digest()).toLowerCase();
        } catch (NoSuchAlgorithmException ex){
            return "Error: SHA-1 algorithm unavailable.";
        }
    }
}
