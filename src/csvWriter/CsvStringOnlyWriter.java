package csvWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class CsvStringOnlyWriter {
    private String filePath;
    private ArrayList<ArrayList<String>> rows;

    public CsvStringOnlyWriter(String filePath){
        this.filePath = filePath;

        this.rows = new ArrayList<>();
    }

    public void addRow(String[] cells){
        ArrayList<String> row = new ArrayList<>();

        for(int i = 0; i < cells.length; i++){
            row.add(cells[i]);
        }

        rows.add(row);
    }

    public void write() throws IOException{
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filePath, false)))){
            // BOM
            out.write("\ufeff");

            for(int j = 0; j < rows.size(); j++){
                out.write(
                    rows.get(j).stream().map(this::stringConvertToCsvValid).collect(Collectors.joining(",")) + "\n"
                );
            }
        }
    }

    private String stringConvertToCsvValid(String s){
        return "\"" + s.replaceAll("\"","\\\\\"") + "\"";
    }
}
