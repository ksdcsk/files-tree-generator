package filesTreeGenerator;

public interface StatusCallbackHandler {
    void filesAnalyzed(int number, int total);
}
