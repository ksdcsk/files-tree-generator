package filesTreeGenerator;

public interface CancelationDispatcher {
    boolean isTaskCanceled();
}
