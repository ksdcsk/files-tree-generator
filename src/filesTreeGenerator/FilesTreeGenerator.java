package filesTreeGenerator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.stream.Stream;

public /*final*/ class FilesTreeGenerator {
    protected String separator;
    protected String startPath;
    protected FileHashCalculator hashCalc = null;
    protected StatusCallbackHandler callbackHandler = null;
    protected CancelationDispatcher cancelationDispatcher = null;

    public FilesTreeGenerator(String path){
        this.startPath = path;

        // Init the default separator
        this.separator = getSystemDefaultSeparator();
    }

    protected String getSystemDefaultSeparator(){
        return File.separator;
    }

    // TODO: Make this option work.
    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public void setHashCalculator(FileHashCalculator fileHashCalc){
        this.hashCalc = fileHashCalc;
    }

    public void setCallbackHandler(StatusCallbackHandler callbackHandler) {
        this.callbackHandler = callbackHandler;
    }

    public void setCancelationDispatcher(CancelationDispatcher cancelationDispatcher) {
        this.cancelationDispatcher = cancelationDispatcher;
    }

    private boolean isCanceled(){
        if(cancelationDispatcher != null){
            return cancelationDispatcher.isTaskCanceled();
        }
        return false;
    }

    public ArrayList<FileInfo> getTree() throws IOException{
        ArrayList<Path> fileList = getFileList();
        ArrayList<FileInfo> fileTree = new ArrayList<>();

        for(int i = 0; i < fileList.size(); i++){
            Path filePath = fileList.get(i);

            String fileName, fileRelativePath, fileHash;

            fileName = filePath.getFileName().toString();
            fileRelativePath = filePath.toString().substring(Paths.get(startPath).toString().length());
            fileHash = hashCalc == null ? "" : hashCalc.computeHashForFile(filePath.toString());

            if(isCanceled()){
                return null;
            }

            fileTree.add(
                    new FileInfo(fileName, fileRelativePath, fileHash)
            );

            if(callbackHandler != null) {
                callbackHandler.filesAnalyzed(i + 1, fileList.size());
            }
        }

        return fileTree;
    }

    protected ArrayList<Path> getFileList() throws IOException{
        Stream<Path> walkResult = Files.walk(Paths.get(startPath));
        ArrayList<Path> fileList = new ArrayList<>();

        walkResult.forEach((Path filePath) -> {
            if(Files.isRegularFile(filePath)
                    && !filePath.getFileName().toString().startsWith(".")){
                fileList.add(filePath);
            }
        });

        fileList.sort(new FileFullPathComparator());

        return fileList;
    }
}

class FileFullPathComparator implements Comparator<Path> {
    @Override
    public int compare(Path o1, Path o2) {
        Iterator<Path> itrObj1 = o1.iterator();
        Iterator<Path> itrObj2 = o2.iterator();

        while(true){
            Path p1 = itrObj1.next();
            Path p2 = itrObj2.next();
            boolean p1Final, p2Final;

            p1Final = itrObj1.hasNext();
            p2Final = itrObj2.hasNext();

            if(p1Final ^ p2Final){ // One is a directory and the other a file
                // Files are listed above directories
                return p1Final ? 1 : -1;
            } else { // Both are either directories or files
                if(p1.compareTo(p2) != 0){
                    return p1.compareTo(p2);
                }
            }
        }
    }
}