package filesTreeGenerator;

import java.io.IOException;

public interface FileHashCalculator {
    String computeHashForFile(String filePath) throws IOException;
}
