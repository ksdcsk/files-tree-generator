package filesTreeGenerator;

public final class FileInfo {
    private String name;
    private String fullPath;
    private String hash;

    public FileInfo(String name, String full_path, String hash){
        this.name = name;
        this.fullPath = full_path;
        this.hash = hash;
    }

    public String getName() {
        return name;
    }

    public String getFullPath() {
        return fullPath;
    }

    public String getHash() {
        return hash;
    }

    @Override
    public String toString() {
        return getName() + " | " + getHash() + " | " + getFullPath();
    }

    public static String[] getFieldsName(){
        return new String[]{"Full path", "File name", "Hash"};
    }

    public String[] getFields(){
        return new String[]{
                fullPath, name, hash
        };
    }
}
