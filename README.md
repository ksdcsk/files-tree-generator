# Files tree generator

This small program searches for files in the indicated by the user path and computes the hash of each file.
The output is a CSV file containing, for each file, its name, its relative path and its hash.

The idea behind this program is to be able to quickly identify identical files.

## Requirements

Developed under Java 8 using Swing.

Tested on Linux, Mac OS and Windows.